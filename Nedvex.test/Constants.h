//
//  Constants.h
//  Nedvex.test
//
//  Created by Eugeniya Pervushina on 14/5/16.
//  Copyright © 2016 Air. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString * const kToken              = @"1d201c40-d24b-4be6-9cae-a4fb0926527b";
static NSString * const kSortName           = @"Name";
static NSString * const kSortCompany        = @"Company";
static NSString * const kSortActivity       = @"Activity";

static NSString * const kApiKeyFirstName    = @"FirstName";
static NSString * const kApiKeyLastName     = @"LastName";
static NSString * const kApiKeyCompany      = @"Company";
static NSString * const kApiKeyLastLogged   = @"LastLoggedInAt";
static NSString * const kApiKeyAvatarFile   = @"AvatarFile";
//
//@interface Constants : NSObject
//
//@end
