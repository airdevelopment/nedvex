//
//  ViewController.h
//  Nedvex.test
//
//  Created by Eugeniya Pervushina on 13/5/16.
//  Copyright © 2016 Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, weak) IBOutlet UISegmentedControl *segmentControl;

@end

