//
//  ViewController.m
//  Nedvex.test
//
//  Created by Eugeniya Pervushina on 13/5/16.
//  Copyright © 2016 Air. All rights reserved.
//

#import "ViewController.h"
#import "TableViewCell.h"
#import "Network.h"
#import "Constants.h"

@interface ViewController () <UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate>
{
    NSMutableArray *array;
    NSString *sortMethod;
    NSInteger top;
    NSInteger skip;
}

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UILabel *usersCountLabel;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self dataSettings];
}

- (void)dataSettings {
    sortMethod = kSortActivity;
    top = 20;
    skip = 20;
    
    [self request];
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

    return [self configuringCell:cell atIndexPath:indexPath];
}

- (TableViewCell *)configuringCell:(TableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dictionary = [array objectAtIndex:indexPath.row];
    NSString *firstName = dictionary[kApiKeyFirstName];
    NSString *lastName = dictionary[kApiKeyLastName];
    NSString *company = dictionary[kApiKeyCompany];
    NSString *date = dictionary[kApiKeyLastLogged];
    if ([firstName isKindOfClass:[NSNull class]]) {
        firstName = @"";
    }
    if ([lastName isKindOfClass:[NSNull class]]) {
        lastName = @"";
    }
    if ([company isKindOfClass:[NSNull class]]) {
        company = @"";
    }
    if ([date isKindOfClass:[NSNull class]]) {
        date = @"";
    }
    
    NSArray* dateArray = [date componentsSeparatedByString: @"T"];
    NSString* firstBit = [dateArray objectAtIndex: 0];
    
    cell.firstNameLabel.text = firstName;
    cell.lastNameLabel.text = lastName;
    cell.companyLabel.text = company;
    cell.lastLoginDateLabel.text = firstBit;
    
    NSString *urlString = (NSString *)dictionary[kApiKeyAvatarFile];
    if (![urlString isKindOfClass:[NSNull class]] && urlString.length > 0) {
        NSString *string = [NSString stringWithFormat:@"%@?token=%@", urlString, kToken];
        UIImage *img = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:string]]];
        cell.userImageView.image = img;
    } else {
        cell.userImageView.image = [UIImage imageNamed:@"defaultAvatar"];
    }
    
    if (indexPath.row == 0 || indexPath.row % 2 == 0) {
        cell.backgroundColor = [UIColor colorWithRed:230/255.0f green:230/255.0f blue:230/255.0f alpha:1.0f];
    } else {
        cell.backgroundColor = [UIColor whiteColor];
    }
    
    return cell;
}

#pragma mark - SegmentControl
- (IBAction)sortMethodChange:(UISegmentedControl *)sender {
    if (sender.selectedSegmentIndex == 0) {
        sortMethod = kSortActivity;
    } else if (sender.selectedSegmentIndex == 1) {
        sortMethod = kSortName;
    } else {
        sortMethod = kSortCompany;
    }
    [self request];
    [self.tableView reloadData];
}

#pragma mark - scroll
- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    if(self.tableView.contentOffset.y >= (self.tableView.contentSize.height - self.tableView.frame.size.height)) {
        top += 20;
        //    skip += 20;
        [self request];
    }
}

#pragma mark - request
-(void)request {
    NSMutableURLRequest *request = [Network getDataWithSortParameter:sortMethod top:top skip:skip];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        
                                                    } else {
                                                        array = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                                                        NSLog(@"request %@", array);
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            _usersCountLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long)array.count];
                                                            
                                                            [self.tableView reloadData];
                                                        });
                                                    }
                                                }];
    [dataTask resume];
}

@end
