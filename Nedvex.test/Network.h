//
//  Network.h
//  Nedvex.test
//
//  Created by Eugeniya Pervushina on 13/5/16.
//  Copyright © 2016 Air. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Network : NSObject

+ (NSMutableURLRequest *)getDataWithSortParameter:(NSString *)parameter top:(NSInteger)top skip:(NSInteger)skip;

@end
