//
//  Network.m
//  Nedvex.test
//
//  Created by Eugeniya Pervushina on 13/5/16.
//  Copyright © 2016 Air. All rights reserved.
//

#import "Network.h"
#import "Constants.h"

@implementation Network

+ (NSMutableURLRequest *)getDataWithSortParameter:(NSString *)parameter top:(NSInteger)top skip:(NSInteger)skip {
    // http://devapp.nedvex.net:84/api/profile/list?token=1d201c40-d24b-4be6-9cae-a4fb0926527b&OrderBy=Name&$top=10

    NSURL *aUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://devapp.nedvex.net:84/api/profile/list?token=%@&OrderBy=%@&$top=%lu", kToken, parameter, top]];
//    NSURL *aUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://devapp.nedvex.net:84/api/profile/list?token=%@&OrderBy=%@&$top=%lu&$skip=%lu", kToken, parameter, top, skip]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:aUrl
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
    
    return request;
}

@end
