//
//  TableViewCell.h
//  Nedvex.test
//
//  Created by Eugeniya Pervushina on 13/5/16.
//  Copyright © 2016 Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *firstNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *lastNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *companyLabel;
@property (nonatomic, weak) IBOutlet UILabel *lastLoginDateLabel;
@property (nonatomic, weak) IBOutlet UIImageView *userImageView;

@end
